#include <stdio.h>

int main() {

    //declaring number of courses, total credits and total gard points.
    int numCourses;
    float totalCredits = 0, totalGradePoints = 0;

    printf("Enter the number of courses: ");
    scanf("%d", &numCourses);
//this is a comment
    // Arrays to store course information
    char names[numCourses][50];
    int creditunits[numCourses];
    float grades[numCourses];

    // Input grades and credit units for each course
    for (int i = 0; i < numCourses; i++) {
        printf("\nEnter the name of Course %d: ", i + 1);
        scanf("%s", names[i]);

        printf("Enter the credit units for Course %d: ", i + 1);
        scanf("%d", &creditunits[i]);

        printf("Enter the grade point for Course %d: ", i + 1);
        scanf("%f", &grades[i]);

        totalCredits += creditunits[i];
        totalGradePoints += grades[i] * creditunits[i];
    }

    // Calculate CGPA
    float cgpa = totalGradePoints / totalCredits;

    // Output CGPA
    printf("\nCGPA is  %.2f\n", cgpa);

    return 0;
}