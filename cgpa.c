#include <stdio.h>
#include <stdlib.h>

// Structure to represent a course
struct Course {
    char name[50];
    int creditunits;
    float grade;
};

int main() {
    int numCourses;
    float totalCredits = 0, totalGradePoints = 0;

    printf("Enter the number of courses: ");
    scanf("%d", &numCourses);

    // Dynamically allocate memory for courses array
    struct Course *courses = malloc(numCourses * sizeof(struct Course));

    // Check if memory allocation succeeded
    if (courses == NULL) {
        printf("Memory allocation failed. Exiting...\n");
        return 1;  // Return non-zero to indicate failure
    }

    // Input grades and credit hours for each course
    for (int i = 0; i < numCourses; i++) {
        printf("\nEnter the name of Course %d: ", i + 1);
        scanf("%s", courses[i].name);

        printf("Enter the credit units for Course %d: ", i + 1);
        scanf("%d", &courses[i].creditunits);

        printf("Enter the grade point for Course %d: ", i + 1);
        scanf("%f", &courses[i].grade);

        totalCredits += courses[i].creditunits;
        totalGradePoints += courses[i].grade * courses[i].creditunits;
    }

    // Calculate CGPA
    float cgpa = totalGradePoints / totalCredits;

    // Output CGPA
    printf("\nCGPA  is %.2f\n", cgpa);

    // Free dynamically allocated memory
    free(courses);

    return 0;
}