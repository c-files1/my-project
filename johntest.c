#include<stdio.h>

    void birthday(char detail[], int age){
        printf("\nHappy birthday dear %s", detail);
        printf("\nYou are now %d years old", age);
    }

    double square(double y){
        return y*y;
    }
int main(){
    
    double y = square(4);
    printf("%.2lf\n",y);

    char detail[25];
    int age;

    printf("Enter name: ");
    scanf("%s", &detail);

    printf("\nEnter age: ");
    scanf("%d", &age);

    birthday(detail, age);

    int numcourses;
    float totalcredits = 0;
    float totalgradepoints = 0;

    printf("\nEnter number of courses: ");
    scanf("%d", &numcourses);

    char name[numcourses][35];
    float grade[numcourses];
    int creditunits[numcourses];

    for(int i = 0; i < numcourses; i++){
        printf("\nEnter course name %d: ", i + 1);
        scanf("%s", &name);

        printf("Enter credit units for %s course %d: ", i + 1, name);
        scanf("%d", &creditunits);

        printf("Enter grade for %s course %d: ", i + 1, name);
        scanf("%f", grade);

        totalcredits += creditunits[i];
        totalgradepoints += grade[i] * creditunits[i];
    }

    float cgpa = totalgradepoints / totalcredits;

    printf("CGPA = %f", cgpa);
    return 0;
}